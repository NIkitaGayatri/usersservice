package org.com.services;

import java.util.List;

import org.com.entity.Users;
import org.com.exception.UserNotFoundException;
import org.com.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
/**
 * This class is used to perform all UserService Operations like getting all
 * users,finding user By Id,adding User,deleting User Updating User
 * 
 */
public class UsersService {
	@Autowired
	UsersRepository usersRepository;

	/**
	 * This method is used to return list of users.
	 * 
	 * @return List<Users> This returns List of all Users.
	 */
	public List<Users> getAllUsers() {
		return usersRepository.findAll();
	}

	/**
	 * This method is used to return user of particular userId
	 * 
	 * @param id This is the first parameter to getUserById method
	 * @return User This returns a User of that particular Id.
	 */

	public Users getUserById(long id) {
		return usersRepository.findById(id).orElseThrow(() -> new UserNotFoundException("USER NOT FOUND WITH ID "));
	}

	/**
	 * This method is used to add a new user
	 * 
	 * @param user This is the first parameter to addUser method
	 */
	public void addUser(Users user) {
		usersRepository.save(user);
	}

	/**
	 * This method is used to Update a user
	 * 
	 * @param id   is the first parameter to specify the user Id to update
	 * @param user This is the second parameter to updateUser method
	 */
	public void updateUser(long id, Users updateUser) {
		Users user = usersRepository.findById(id)
				.orElseThrow(() -> new UserNotFoundException("USER NOT FOUND WITH ID "));
		user.setEmailId(updateUser.getEmailId());
		user.setUserId(updateUser.getUserId());
		user.setUserName(updateUser.getUserName());
		usersRepository.save(user);

	}

	/**
	 * This method is used to delete a particular user
	 * 
	 * @param id This is the first parameter to deleteUser method
	 */
	public void deleteUser(long id) {
		Users user = usersRepository.findById(id)
				.orElseThrow(() -> new UserNotFoundException("USER NOT FOUND WITH ID "));
		usersRepository.delete(user);
	}

}
