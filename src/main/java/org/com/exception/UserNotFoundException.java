package org.com.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * This class is used to execute when an 
 * user is not found raising an exception 
 * user not found
 * 
 */
@SuppressWarnings("serial")
public class UserNotFoundException extends RuntimeException {
	private static final Logger log = LogManager.getLogger(UserNotFoundException.class);

	public UserNotFoundException(String message) {
		super(message);
		log.info(message);
	}
}
