package org.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * The UserServiceRestApplication that implements all RestFul services like
 * Displaying all users,adding user,removing user updating user
 *
 * @author NikitaGayatri Kandukuri
 * @version 1.0
 * @since 2020-04-01
 */
@SpringBootApplication
@EnableDiscoveryClient
public class UserServiceRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserServiceRestApplication.class, args);
	}

}
