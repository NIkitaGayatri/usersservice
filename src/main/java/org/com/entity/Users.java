package org.com.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Users {
	@Id
	@ApiModelProperty(notes="Specific Id of the user")
	long userId;
	@ApiModelProperty(notes="Name of the user")
	String userName;
	@ApiModelProperty(notes="EmailId of the user")
	String emailId;
	String mobile;
}
