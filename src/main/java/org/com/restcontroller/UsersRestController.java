package org.com.restcontroller;

import java.util.List;

import org.com.entity.Users;
import org.com.exception.UserNotFoundException;
import org.com.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/")
@Api(description = "This is simple UserService we can use this api to add,read,update and delete user" )
@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully added new book"),
		@ApiResponse(code = 401, message = "Unauthorized", response = UserNotFoundException.class),
		@ApiResponse(code = 403, message = "Forbidden", response = UserNotFoundException.class),
		@ApiResponse(code = 404, message = "The resource you are trying is not found", response = UserNotFoundException.class),
		@ApiResponse(code = 500, message = "Internal Sever error", response = UserNotFoundException.class) })
public class UsersRestController {
	@Autowired
	UsersService usersService;
	@GetMapping("users")
	@ApiOperation(value = "List of all users", response = Iterable.class)
	public ResponseEntity<List<Users>> getAllUsers() {
		return new ResponseEntity<List<Users>>(usersService.getAllUsers(), HttpStatus.OK);
	}

	@GetMapping("users/{user_id}")
	@ApiOperation(value = "Get user by id", response = Users.class)
	public ResponseEntity<Users> getuserById(@PathVariable long user_id) {
		return new ResponseEntity<Users>(usersService.getUserById(user_id), HttpStatus.OK);

	}
	
	@PostMapping(value = "users")
	@ApiOperation(value = "Adding a new user", response = Users.class)
	public ResponseEntity<Users> addUser(
			@ApiParam(value = "A new user addition ", required = true) @RequestBody Users user) {
		usersService.addUser(user);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@DeleteMapping("users/{user_id}")
	@ApiOperation(value = "Deleting a user", response = Users.class)
	public ResponseEntity<?> deleteUser(@PathVariable long user_id) {
		usersService.deleteUser(user_id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PutMapping("users/{user_id}")
	@ApiOperation(value = "Updating a user", response = Users.class)
	public ResponseEntity<Users> updateUser(@PathVariable long user_id, @RequestBody Users user) {
		usersService.updateUser(user_id, user);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

}
