package org.com.repositorytest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Optional;

import org.com.entity.Users;
import org.com.repository.UsersRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class UsersRepositoryTest {
	@Autowired
	UsersRepository usersRepository;

	@Test
	void testSaveBook() {
		Users user = new Users();
		user.setUserId(1L);
		user.setEmailId("gayatri@gmail.com");
		user.setUserName("gayatri");
		usersRepository.save(user);
		assertEquals("gayatri", user.getUserName());
	}

	@Test
	void testFindAll() {
		Users user = new Users(1, "gayatri", "gayatri@gmail.com", "901091010");
		usersRepository.save(user);
		List<Users> bookList = usersRepository.findAll();
		assertThat(bookList).isNotEmpty();
	}

	@Test
	void testDelete() {
		Users user = new Users(1, "gayatri", "gayatri@gmail.com", "901091010");
		usersRepository.save(user);
		Optional<Users> optional = usersRepository.findById(1L);
		Users user2 = optional.get();
		assertEquals(1, user.getUserId());
		usersRepository.delete(user2);
	}

	@Test
	void testFindById() {
		Users user = new Users(1, "gayatri", "gayatri@gmail.com", "901091010");
		usersRepository.save(user);
		Optional<Users> optional = usersRepository.findById(1L);
		Users user2 = optional.get();
		assertEquals("gayatri", user2.getUserName());
	}

}
