package org.com.restcontrollertest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.com.entity.Users;
import org.com.restcontroller.UsersRestController;
import org.com.services.UsersService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

class UsersRestControllerTest {
	@InjectMocks
	UsersRestController usersController;
	@Mock 
	UsersService usersService;
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	@Test
	public void testGetAllUsers() {
		List<Users>userList=new ArrayList<>();
		userList.add(new Users(1,"gayatri","gayatri@gmail.com","901091010"));
		when(usersService.getAllUsers()).thenReturn(userList);
		ResponseEntity<List<Users>> response = usersController.getAllUsers();
		assertThat(response.getStatusCodeValue()).isEqualTo(200);
		assertThat(response.getBody().size()).isEqualTo(userList.size());
		assertEquals(response.getBody().get(0).getUserName(), userList.get(0).getUserName());
		assertNotEquals(201, response.getStatusCodeValue());		
	}
	@Test
	public void testGetUserById() {
		Users user=new Users(1,"gayatri","gayatri@gmail.com","901091010");
		when(usersService.getUserById(Mockito.anyInt())).thenReturn(user);
		ResponseEntity<Users>response=usersController.getuserById(1L);
		assertThat(response.getStatusCodeValue()).isEqualTo(200);
	}
	@Test
	public void testAddBook() {
		Users user=new Users(1,"gayatri","gayatri@gmail.com","901091010");
		usersService.addUser(Mockito.any());
		ResponseEntity<Users>response=usersController.addUser(user);
		assertThat(response.getStatusCodeValue()).isEqualTo(201);
	}
	@Test
	public void testDeleteBook() {
		Users user=new Users(1,"gayatri","gayatri@gmail.com","901091010");
		ResponseEntity<Users>response=(ResponseEntity<Users>) usersController.deleteUser(1L);
		assertThat(response.getStatusCodeValue()).isEqualTo(204);
	}
	@Test
	public void testUpdateBook() {
		Users user=new Users(1,"gayatri","gayatri@gmail.com","901091010");
		usersService.updateUser(Mockito.anyInt(),Mockito.any());
		ResponseEntity<Users>response=(ResponseEntity<Users>) usersController.updateUser(1,user);
		assertThat(response.getStatusCodeValue()).isEqualTo(201);
	}


}
