package org.com.servicetest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.com.entity.Users;
import org.com.exception.UserNotFoundException;
import org.com.repository.UsersRepository;
import org.com.services.UsersService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

class UsersServiceTest {

	@InjectMocks
	UsersService usersService;
	@Mock
	UsersRepository usersRepository;
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetAllusers() {
		List<Users>usersList=new ArrayList<>();
		usersList.add(new Users(1,"gayatri","gayatri@gmail.com","901091010"));
		usersList.add(new Users(2,"vyshu","vyshu@gmail.com","90191010"));
		when(usersService.getAllUsers()).thenReturn(usersList);
	}
	@Test
	public void getusersIdTest() throws UserNotFoundException {
		Users user=new Users(1,"gayatri","gayatri@gmail.com","901091010");
		Optional<Users> optional=Optional.of(user);
		when(usersRepository.findById(Mockito.anyLong())).thenReturn(optional);
		usersService.getUserById(1L);
	}
	@Test
	public void createusersTest() {
		Users user=new Users(1,"gayatri","gayatri@gmail.com","901091010");
		when(usersRepository.save(Mockito.any())).thenReturn(user);
		usersService.addUser(Mockito.any());
	}
	
	@Test
	public void deleteusersTest() throws UserNotFoundException {
		Users user=new Users(1,"gayatri","gayatri@gmail.com","901091010");
	    Optional<Users> optional=Optional.of(user);
		when(usersRepository.findById(Mockito.anyLong())).thenReturn(optional);
		usersService.deleteUser(Mockito.anyInt());
	}
	@Test
	public void updateusersTest() throws UserNotFoundException {
		Users user=new Users(1,"gayatri","gayatri@gmail.com","901091010");
	    Optional<Users> optional=Optional.of(user);
		when(usersRepository.findById(Mockito.anyLong())).thenReturn(optional);	
		usersService.updateUser(1,user);
	}
	@Test
    void exceptionTestForDelete() {
        Users users = null;
        Optional<Users> optional= Optional.ofNullable(users) ;
        when(usersRepository.findById(Mockito.anyLong())).thenReturn(optional);
        UserNotFoundException thrown = assertThrows(UserNotFoundException.class, () -> usersService.deleteUser(Mockito.anyLong()), "USER NOT FOUND WITH ID ");
        assertTrue(thrown.getMessage().contains("USER NOT FOUND WITH ID "));
    }
	@Test
    void exceptionTestusersById() {
        Users users = null;
        Optional<Users> optional= Optional.ofNullable(users) ;
        when(usersRepository.findById(Mockito.anyLong())).thenReturn(optional);
        UserNotFoundException thrown = assertThrows(UserNotFoundException.class, () -> usersService.getUserById(Mockito.anyLong()), "USER NOT FOUND WITH ID ");
        assertTrue(thrown.getMessage().contains("USER NOT FOUND WITH ID "));
    }

}
